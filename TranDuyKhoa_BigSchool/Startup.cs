﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TranDuyKhoa_BigSchool.Startup))]
namespace TranDuyKhoa_BigSchool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
